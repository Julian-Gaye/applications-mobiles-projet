package fr.uavignon.ceri.projet.data.webservice;

import java.util.List;
import java.util.Map;

public class CollectionResponse {

        public String name = null;
        public List<String> categories = null;
        public String description = null;
        public List<Integer> timeFrame = null;
        public Integer year = null;
        public String brand = null;
        public List<String> technicalDetails = null;
        public Map<String, String> pictures = null;
        public Boolean working = null;
}
