package fr.uavignon.ceri.projet;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import org.w3c.dom.Text;

public class CollectionFragment extends Fragment {

    public static final String TAG = CollectionFragment.class.getSimpleName();
    public static final String SORT = "Sort";
    public static final String LAST_SEARCH = "LastSearch";
    public static final String FAVORITE_FIRST = "FavoriteFirst";

    private CollectionViewModel viewModel;
    private int sort;
    private boolean favoriteFirst;
    private String lastSearch;
    private Menu menu;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter adapter;
    private SharedPreferences sharedPreferences;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        MainActivity activity = (MainActivity)getActivity();
        activity.setMenuMenu();
        sharedPreferences = getContext().getSharedPreferences(MainActivity.SHARED_PREFS, Context.MODE_PRIVATE);
        sort = sharedPreferences.getInt(SORT, RecyclerAdapter.SORT_BY_NAME);
        favoriteFirst = sharedPreferences.getBoolean(FAVORITE_FIRST, false);
        lastSearch = sharedPreferences.getString(LAST_SEARCH, "");
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_collection, container, false);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        this.menu = menu;
        inflater.inflate(R.menu.menu_collection, menu);

        if(favoriteFirst) {
            MenuItem favoriteFirstButton = menu.findItem(R.id.favorite_first);
            favoriteFirstButton.setTitle("Ne plus afficher les favoris en premier");
        }

        SpannableString spannableString;
        MenuItem menuItem;
        if(sort == RecyclerAdapter.SORT_BY_NAME) {
            menuItem = menu.findItem(R.id.sort_alphabetic);
            spannableString = new SpannableString(menuItem.getTitle().toString());
            spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 0, spannableString.length(), 0);
            menuItem.setTitle(spannableString);
        }
        else if(sort == RecyclerAdapter.SORT_BY_POPULARITY) {
            menuItem = menu.findItem(R.id.sort_popularity);
            spannableString = new SpannableString(menuItem.getTitle().toString());
            spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 0, spannableString.length(), 0);
            menuItem.setTitle(spannableString);
        }
        else if(sort == RecyclerAdapter.SORT_BY_CHRONOLOGY) {
            menuItem = menu.findItem(R.id.sort_chronology);
            spannableString = new SpannableString(menuItem.getTitle().toString());
            spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 0, spannableString.length(), 0);
            menuItem.setTitle(spannableString);
        }
        MenuItem searchButton = menu.findItem(R.id.button_search);
        SearchView searchView = (SearchView) searchButton.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        if(lastSearch != null && lastSearch.trim().length() > 0) { //On garde la recherche quand on ouvre les détails et qu'on revient sur la liste
            searchButton.expandActionView();
            searchView.setQuery(lastSearch, true);
            adapter.getFilter().filter(lastSearch);
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                lastSearch = newText;
                return false;
            }
        });
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(CollectionViewModel.class);
        listenerSetup();
        observerSetup();
    }

    private void listenerSetup() {
        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        adapter.setViewModel(viewModel);
        adapter.setSharedPreference(sharedPreferences);
        recyclerView.setAdapter(adapter);
    }

    private void observerSetup() {
        viewModel.getAllItems().observe(getViewLifecycleOwner(),
                items -> {
                    adapter.setItemCatalogue(items, sort, favoriteFirst);
                });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        SharedPreferences.Editor editor = sharedPreferences.edit();

        SpannableString spannableString;

        MenuItem menuItem;

        switch(id) {
            case R.id.sort_alphabetic:
                editor.putInt(SORT, RecyclerAdapter.SORT_BY_NAME);

                menuItem = menu.findItem(R.id.sort_popularity);
                spannableString = new SpannableString(menuItem.getTitle().toString());
                spannableString.setSpan(null, 0, spannableString.length(), 0);
                menuItem.setTitle(spannableString);

                menuItem = menu.findItem(R.id.sort_chronology);
                spannableString = new SpannableString(menuItem.getTitle().toString());
                spannableString.setSpan(null, 0, spannableString.length(), 0);
                menuItem.setTitle(spannableString);

                spannableString = new SpannableString(item.getTitle().toString());
                spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 0, spannableString.length(), 0);
                item.setTitle(spannableString);

                adapter.sortByName();
                break;
            case R.id.sort_popularity:
                editor.putInt(SORT, RecyclerAdapter.SORT_BY_POPULARITY);

                menuItem = menu.findItem(R.id.sort_alphabetic);
                spannableString = new SpannableString(menuItem.getTitle().toString());
                spannableString.setSpan(null, 0, spannableString.length(), 0);
                menuItem.setTitle(spannableString);

                menuItem = menu.findItem(R.id.sort_chronology);
                spannableString = new SpannableString(menuItem.getTitle().toString());
                spannableString.setSpan(null, 0, spannableString.length(), 0);
                menuItem.setTitle(spannableString);

                spannableString = new SpannableString(item.getTitle().toString());
                spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 0, spannableString.length(), 0);
                item.setTitle(spannableString);

                adapter.sortByPopularity();
                break;
            case R.id.sort_chronology:
                editor.putInt(SORT, RecyclerAdapter.SORT_BY_CHRONOLOGY);

                menuItem = menu.findItem(R.id.sort_alphabetic);
                spannableString = new SpannableString(menuItem.getTitle().toString());
                spannableString.setSpan(null, 0, spannableString.length(), 0);
                menuItem.setTitle(spannableString);

                menuItem = menu.findItem(R.id.sort_popularity);
                spannableString = new SpannableString(menuItem.getTitle().toString());
                spannableString.setSpan(null, 0, spannableString.length(), 0);
                menuItem.setTitle(spannableString);

                spannableString = new SpannableString(item.getTitle().toString());
                spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 0, spannableString.length(), 0);
                item.setTitle(spannableString);

                adapter.sortByChronology();
                break;
            case R.id.favorite_first:
                boolean newValue = !sharedPreferences.getBoolean(FAVORITE_FIRST, false);
                if(newValue == false) {
                    item.setTitle("Afficher les favoris en premier");
                    adapter.favoriteNotFirst();
                }
                else {
                    item.setTitle("Ne plus afficher les favoris en premier");
                    adapter.favoriteFirst();
                }
                favoriteFirst = newValue;
                editor.putBoolean(FAVORITE_FIRST, newValue);
                break;
        }
        editor.commit();
        sort = sharedPreferences.getInt(SORT, RecyclerAdapter.SORT_BY_NAME);
        return super.onOptionsItemSelected(item);
    }
}