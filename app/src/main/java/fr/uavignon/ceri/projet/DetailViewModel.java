package fr.uavignon.ceri.projet;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.projet.data.CerimuseumRepository;
import fr.uavignon.ceri.projet.data.database.entities.Item;

public class DetailViewModel extends AndroidViewModel {

    private CerimuseumRepository repository;
    private MutableLiveData<Item> item;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;

    public DetailViewModel (Application application) {
        super(application);
        repository = CerimuseumRepository.get(application);
        item = new MutableLiveData<>();
        isLoading = repository.isLoading();
        webServiceThrowable = repository.getError();
    }

    public void setItem(String id) {
        repository.getItem(id);
        item = repository.getSelectedItem();
    }

    MutableLiveData<Item> getItem() {
        return item;
    }

    public void addFavorite(Item item) {

        item.addFavorite();
        repository.like(item);
        repository.updateItem(item);
    }

    public void removeFavorite(Item item) {

        item.removeFavorite();
        repository.dislike(item);
        repository.updateItem(item);
    }
}
