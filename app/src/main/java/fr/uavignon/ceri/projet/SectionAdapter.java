package fr.uavignon.ceri.projet;

import android.app.Activity;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import fr.uavignon.ceri.projet.data.Section;
import fr.uavignon.ceri.projet.data.database.entities.Categorie;
import fr.uavignon.ceri.projet.data.database.entities.Item;

public class SectionAdapter extends RecyclerView.Adapter<SectionAdapter.ViewHolder> {
    private List<Categorie> categorieList;
    private List<Item> itemList;
    private List<Section> sectionList;
    private RecyclerAdapter itemAdapter;
    private Activity activity;
    private CollectionViewModel viewModel;
    private SharedPreferences sharedPreferences;

    public SectionAdapter(Activity activity) {
        super();
        this.activity = activity;
        sectionList = new ArrayList<>();
    }

    @NonNull
    @Override
    public SectionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.section_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(sectionList.size() == 0)
            return;
        Section section = sectionList.get(position);
        holder.sectionName.setText(section.getCategorie().getName());
        // RecyclerView for items
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        holder.itemRecyclerView.setLayoutManager(linearLayoutManager);
        itemAdapter = new RecyclerAdapter();
        itemAdapter.setItemCatalogue(section.getAllItemsInCategorie(), RecyclerAdapter.SORT_BY_NAME, false);
        itemAdapter.setViewModel(viewModel);
        itemAdapter.setSharedPreference(sharedPreferences);
        holder.itemRecyclerView.setAdapter(itemAdapter);
    }

    @Override
    public int getItemCount() {
        return categorieList == null ? 0 : categorieList.size();
    }

    public void setCategorieList(List<Categorie> categories) {
        categorieList = categories;
        setSectionList();
        notifyDataSetChanged();
    }

    public void setItemList(List<Item> items) {
        itemList = items;
        setSectionList();
        notifyDataSetChanged();
    }

    private void setSectionList() {
        sectionList.clear();
        if(categorieList == null || itemList == null)
            return;
        for(Categorie categorie : categorieList) {
            List<Item> items = new ArrayList<>();
            for(Item item : itemList) {
                for(String s : item.getCategories()) {
                    if(s.equals(categorie.getName())) {
                        items.add(item);
                        break;
                    }
                }
            }
            Section section = new Section(categorie, items);
            sectionList.add(section);
        }
    }

    public void setViewModel(CollectionViewModel viewModel) {

        this.viewModel = viewModel;
    }

    public void setSharedPreferences(SharedPreferences sharedPreferences) {

        this.sharedPreferences = sharedPreferences;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView sectionName;
        RecyclerView itemRecyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sectionName = itemView.findViewById(R.id.section_item_text_view);
            itemRecyclerView = itemView.findViewById(R.id.item_recycler_view);
        }
    }
}