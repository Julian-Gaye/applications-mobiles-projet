package fr.uavignon.ceri.projet.data.database.entities.relations;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "items_categories_crossRef", primaryKeys = {"itemId", "categorieName"})
public class ItemsAndCatogoriesCrossRef {

    @NonNull
    @ColumnInfo(name = "itemId")
    private String itemId;

    @ColumnInfo(name = "categorieName")
    private int categorieName;

    public ItemsAndCatogoriesCrossRef(@NonNull String itemId, int categorieName) {
        this.itemId = itemId;
        this.categorieName = categorieName;
    }

    @NonNull
    public String getItemId() {
        return itemId;
    }

    public int getCategorieName() {
        return categorieName;
    }

    public void setItemId(@NonNull String itemId) {
        this.itemId = itemId;
    }

    public void setCategorieName(int categorieName) {
        this.categorieName = categorieName;
    }
}
