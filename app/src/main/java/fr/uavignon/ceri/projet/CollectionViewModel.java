package fr.uavignon.ceri.projet;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.projet.data.CerimuseumRepository;
import fr.uavignon.ceri.projet.data.database.entities.Item;

public class CollectionViewModel extends AndroidViewModel {

    private CerimuseumRepository repository;
    private LiveData<List<Item>> allItems;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;

    public CollectionViewModel(@NonNull Application application) {
        super(application);
        repository = CerimuseumRepository.get(application);
        allItems = repository.getAllItems();
        isLoading = repository.isLoading();
        webServiceThrowable = repository.getError();
    }

    public LiveData<List<Item>> getAllItems() {

        return allItems;
    }

    public void addFavorite(Item item) {

        item.addFavorite();
        repository.like(item);
        repository.updateItem(item);
    }

    public void removeFavorite(Item item) {

        item.removeFavorite();
        repository.dislike(item);
        repository.updateItem(item);
    }
}
