package fr.uavignon.ceri.projet.data.database.entities.relations;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

import fr.uavignon.ceri.projet.data.database.entities.Categorie;
import fr.uavignon.ceri.projet.data.database.entities.Item;
import fr.uavignon.ceri.projet.data.database.entities.relations.ItemsAndCatogoriesCrossRef;

public class ItemWithCategorie {
    @Embedded
    public Item item;
    @Relation(
            parentColumn = "itemId",
            entityColumn = "categorieName",
            associateBy = @Junction(ItemsAndCatogoriesCrossRef.class)
    )
    public List<Categorie> categories;
}
