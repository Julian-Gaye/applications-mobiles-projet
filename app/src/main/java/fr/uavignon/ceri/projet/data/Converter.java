package fr.uavignon.ceri.projet.data;

import androidx.room.TypeConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Converter {

    @TypeConverter
    public static String stringListToStringOfStrings(List<String> list) {

        if(list == null)
            return null;

        String str = "";
        Boolean addSeparator = false;
        for(String s : list) {

            if(addSeparator == false)
                addSeparator = true;
            else
                str += ";";
            str += s;
        }
        return str;
    }

    @TypeConverter
    public static List<String> stringOfStringsToStringList(String strings) {

        if(strings == null)
            return null;

        List<String> list = new ArrayList<>();
        int index = strings.indexOf(";");
        while(index != -1) {

            list.add(strings.substring(0, index));
            strings = strings.substring(index + 1);
            index = strings.indexOf(";");
        }
        list.add(strings);
        return list;
    }

    @TypeConverter
    public static String integersListToStringOfIntegers(List<Integer> list) {

        if(list == null)
            return null;

        String str = "";
        Boolean addSeparator = false;
        for(Integer s : list) {

            if(addSeparator == false)
                addSeparator = true;
            else
                str += ";";
            str += s;
        }
        return str;
    }

    @TypeConverter
    public static List<Integer> stringOfIntegersToIntegerList(String integers) {

        if(integers == null)
            return null;

        List<Integer> list = new ArrayList<>();
        int index = integers.indexOf(';');
        while(index != -1) {

            list.add(Integer.parseInt(integers.substring(0, index)));
            integers = integers.substring(index + 1);
            index = integers.indexOf(';');
        }
        list.add(Integer.parseInt(integers));
        return list;
    }

    @TypeConverter
    public static String stringMapToStringOfStrings(Map<String, String> map) {

        if(map == null)
            return null;

        String str = "";
        Boolean addSeparator = false;
        for(Map.Entry<String, String> entry : map.entrySet()) {

            if(addSeparator == false)
                addSeparator = true;
            else
                str += ";";
            str += entry.getKey() + ":" + entry.getValue();
        }
        return str;
    }

    @TypeConverter
    public static Map<String, String> stringOfStringsToStringMap(String strings) {

        if(strings == null)
            return null;

        Map<String, String> map = new HashMap<>();
        int index = strings.indexOf(';');
        while(index != -1) {

            String s = strings.substring(0, index);
            map.put(s.substring(0, s.indexOf(':')), s.substring(s.indexOf(':') + 1));
            strings = strings.substring(index + 1);
            index = strings.indexOf(';');
        }
        map.put(strings.substring(0, strings.indexOf(':')), strings.substring(strings.indexOf(':') + 1));
        return map;
    }
}
