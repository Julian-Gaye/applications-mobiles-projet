package fr.uavignon.ceri.projet.data;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import fr.uavignon.ceri.projet.data.database.CerimuseumDao;
import fr.uavignon.ceri.projet.data.database.CerimuseumRoomDatabase;
import fr.uavignon.ceri.projet.data.database.entities.Categorie;
import fr.uavignon.ceri.projet.data.database.entities.Item;
import fr.uavignon.ceri.projet.data.webservice.CerimuseumInterface;
import fr.uavignon.ceri.projet.data.webservice.CollectionResponse;
import fr.uavignon.ceri.projet.data.webservice.ApiResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.projet.data.database.CerimuseumRoomDatabase.databaseWriteExecutor;

public class CerimuseumRepository {

    private static final String TAG = CerimuseumRepository.class.getSimpleName();
    private static final String SHARED_PREFS = "SharedPrefs";
    private static final String COLLECTION_VERSION = "CollectionVersion";
    private static final String POPULARITY_VERSION = "PopularityVersion";

    private LiveData<List<Item>> allItems;
    private LiveData<List<Categorie>> allCategories;
    private MutableLiveData<Item> selectedItem;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Integer> updateAvailable; //0 : inconnu, 1 : vrai, 2 : faux
    private MutableLiveData<Throwable> webServiceThrowable;
    private Application application;

    private CerimuseumRepository repository = this;

    private CerimuseumDao cerimuseumDao;
    private final CerimuseumInterface api;

    private static volatile CerimuseumRepository INSTANCE;

    public synchronized static CerimuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new CerimuseumRepository(application);
        }
        return INSTANCE;
    }

    public CerimuseumRepository(Application application) {
        CerimuseumRoomDatabase db = CerimuseumRoomDatabase.getDatabase(application);
        cerimuseumDao = db.cerimuseumDao();
        allItems = cerimuseumDao.getAllItems();
        allCategories = cerimuseumDao.getAllCategories();
        selectedItem = new MutableLiveData<>();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://demo-lia.univ-avignon.fr/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build();
        api = retrofit.create(CerimuseumInterface.class);
        isLoading = new MutableLiveData<>();
        webServiceThrowable = new MutableLiveData<>();
        updateAvailable = new MutableLiveData<>();
        this.application = application;
    }

    public void loadItems() {
        updateAvailable.postValue(0);
        SharedPreferences sharedPreferences = application.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        float collectionVersion = sharedPreferences.getFloat(COLLECTION_VERSION, 0.0f); //On récupère la version de l'appareil
        Log.d("VersionCollectionApp", String.valueOf(collectionVersion));
        webServiceThrowable.postValue(null);
        api.getCollectionVersion().enqueue(
                new Callback<Float>() {
                    @Override
                    public void onResponse(Call<Float> call, Response<Float> response) {

                        if(response.body() != collectionVersion) { //Si la version sur l'appareil est différente de la version sur le serveur
                            isLoading.postValue(true);
                            loadCategories();
                            float newVersion = response.body(); //On récupère la nouvelle version pour la changer une fois que la mise à jour a été appliquée (pour éviter d'avoir un problème si on quitte l'application pendant la mise à jour)
                            Log.d("Collection", "Mise à jour de la collection");
                            api.getCollection().enqueue(
                                    new Callback<Map<String, CollectionResponse>>() {

                                        @Override
                                        public void onResponse(Call<Map<String, CollectionResponse>> call, Response<Map<String, CollectionResponse>> response) {
                                            ApiResult.updateCollection(response.body(), repository, allItems.getValue());
                                            if(selectedItem.getValue() != null)
                                                getItem(selectedItem.getValue().getId()); //On recharge l'item actuellement selectionné
                                            isLoading.postValue(false);
                                            updateAvailable.postValue(1);
                                            editor.putFloat(COLLECTION_VERSION, newVersion); //On change la version sur l'appareil
                                            editor.commit();
                                            loadPopularity(); //On ne charge la popularité qu'une fois que tous les items sont chargés
                                        }

                                        @Override
                                        public void onFailure(Call<Map<String, CollectionResponse>> call, Throwable t) {
                                            Log.e("Erreur collection", t.toString());
                                            webServiceThrowable.postValue(t);
                                            isLoading.postValue(false);
                                        }
                                    });
                        }
                        else {
                            Log.d("Collection", "Données déjà à jour");
                            updateAvailable.postValue(2);
                            loadPopularity(); //On ne charge la popularité qu'une fois que tous les items sont chargés
                        }
                    }

                    @Override
                    public void onFailure(Call<Float> call, Throwable t) {
                        Log.e("Erreur collection", t.toString());
                        webServiceThrowable.postValue(t);
                    }
                }
        );
    }

    public void loadCategories() {
        api.getCategories().enqueue(
                new Callback<List<String>>() {
                    @Override
                    public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                        ApiResult.updateCategories(response.body(), repository);
                    }

                    @Override
                    public void onFailure(Call<List<String>> call, Throwable t) {
                        Log.e("Erreur categories", t.toString());
                    }
                }
        );
    }

    public void loadPopularity() {
        SharedPreferences sharedPreferences = application.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        float popularityVersion = sharedPreferences.getFloat(POPULARITY_VERSION, 0.0f);
        Log.d("VersionPopularityApp", String.valueOf(popularityVersion));
        api.getPopularityVersion().enqueue(
                new Callback<Float>() {
                    @Override
                    public void onResponse(Call<Float> call, Response<Float> response) {
                        if(response.body() != popularityVersion) {
                            float newVersion = response.body(); //On récupère la nouvelle version pour la changer une fois que la mise à jour a été appliquée (pour éviter d'avoir un problème si on quitte l'application pendant la mise à jour)
                            Log.d("Popularité", "Mise à jour de la popularité");
                            api.getPopularity().enqueue(
                                    new Callback<Map<String, Long>>() {
                                        @Override
                                        public void onResponse(Call<Map<String, Long>> call, Response<Map<String, Long>> response) {
                                            ApiResult.updatePopularity(response.body(), repository, allItems.getValue());
                                            editor.putFloat(POPULARITY_VERSION, newVersion); //On change la version de l'appareil
                                            editor.commit();
                                        }

                                        @Override
                                        public void onFailure(Call<Map<String, Long>> call, Throwable t) {
                                            Log.e("Erreur popularité", t.toString());
                                        }
                                    }
                            );
                        }
                        else
                            Log.d("Popularity", "Données déjà à jour");
                    }

                    @Override
                    public void onFailure(Call<Float> call, Throwable t) {
                        Log.e("Erreur popularité", t.toString());
                    }
                }
        );
    }

    public void saveAllThumbnailAndPictures(List<Item> itemList) {
        long time = System.currentTimeMillis();
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        for(Item item : itemList) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        saveThumbnailAndPictures(item);
                    }
                });
        }
        executor.shutdown();
        try {
            executor.awaitTermination(10, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d("loading duration", "Temps pour tout charger : " + (System.currentTimeMillis() - time) + " ms");
    }

    public void saveThumbnailAndPictures(Item item) {
        saveThumbnail(item);
        savePictures(item);
    }

    public void saveThumbnail(Item item) {
        String url = "https://demo-lia.univ-avignon.fr/cerimuseum/items/" + item.getId() + "/thumbnail";
        getAndSaveImage(url, "thumbnails" ,"thumbnail_" + item.getId() + ".png");
    }

    public void savePictures(Item item) {

        Map<String, String> pictures = item.getPictures();
        if(pictures == null)
            return;
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        for(Map.Entry entry : pictures.entrySet()) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    String url = "https://demo-lia.univ-avignon.fr/cerimuseum/items/" + item.getId() + "/images/" + entry.getKey();
                    getAndSaveImage(url, "pictures/" + item.getId(), entry.getKey() + ".jpeg");
                }
            });
        }
        executor.shutdown();
        try {
            executor.awaitTermination(10, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void getAndSaveImage(String url, String directory, String filename) {
        Bitmap bitmap = getBitmapFromUrl(url);
        saveImage(bitmap, directory, filename);
    }

    private Bitmap getBitmapFromUrl(String url) {
        Context context = application.getApplicationContext();
        try {
            return Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .submit().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String saveImage(Bitmap image, String directory, String fileName) {
        String savedImagePath = null;
        Context context = application.getApplicationContext();
        File storageDir = new File(context.getExternalCacheDir()
                + "/" + directory);
        boolean success = true;
        if (!storageDir.exists()) {
            success = storageDir.mkdirs();
        }
        if (success) {
            File imageFile = new File(storageDir, fileName);
            savedImagePath = imageFile.getAbsolutePath();
            try {
                OutputStream fOut = new FileOutputStream(imageFile);
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            storageAddPic(savedImagePath);
        }
        return savedImagePath;
    }

    private void storageAddPic(String imagePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(imagePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        application.sendBroadcast(mediaScanIntent);
    }

    public void like(Item item) {

        api.like(item.getId()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("Like",item.getId() + " liké");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("Erreur like", "Erreur lors du like de " + item.getId());
            }
        });
    }

    public void dislike(Item item) {

        api.dislike(item.getId()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("Dislike",item.getId() + " disliké");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("Erreur dislike", "Erreur lors du dislike de " + item.getId());
            }
        });
    }

    public LiveData<List<Item>> getAllItems() {
        return allItems;
    }

    public LiveData<List<Categorie>> getAllCategories() {
        return allCategories;
    }

    public MutableLiveData<Item> getSelectedItem() {
        return selectedItem;
    }

    public void insertItem(Item item) {
        databaseWriteExecutor.submit(() -> {
            return cerimuseumDao.insertItem(item);
        });
    }

    public void updateItem(Item item) {
        databaseWriteExecutor.submit(() -> {
            return cerimuseumDao.update(item);
        });
    }

    public void deleteItem(Item item) {
        databaseWriteExecutor.submit(() -> {
            cerimuseumDao.deleteItem(item.getId());
        });
    }

    public void getItem(String id)  {
        Future<Item> fitem = databaseWriteExecutor.submit(() -> {
            return cerimuseumDao.getItemById(id);
        });
        try {
            selectedItem.setValue(fitem.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void insertCategorie(Categorie categorie) {
        databaseWriteExecutor.submit(() -> {
            return cerimuseumDao.insertCategorie(categorie);
        });
    }

    public MutableLiveData<Boolean> isLoading() {

        return isLoading;
    }


    public MutableLiveData<Throwable> getError() {

        return webServiceThrowable;
    }

    public MutableLiveData<Integer> getUpdateAvailable() {

        return updateAvailable;
    }
}
