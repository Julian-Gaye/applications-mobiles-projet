package fr.uavignon.ceri.projet.data;

import java.util.List;

import fr.uavignon.ceri.projet.data.database.entities.Categorie;
import fr.uavignon.ceri.projet.data.database.entities.Item;

public class Section {

    private Categorie categorie;
    private List<Item> allItemsInCategorie;

    public Section(Categorie categorie, List<Item> allItemsInCategorie) {
        this.categorie = categorie;
        this.allItemsInCategorie = allItemsInCategorie;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public List<Item> getAllItemsInCategorie() {
        return allItemsInCategorie;
    }

    public void setAllItemsInCategorie(List<Item> allItemsInSection) {
        this.allItemsInCategorie = allItemsInSection;
    }
}