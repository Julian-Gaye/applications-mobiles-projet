package fr.uavignon.ceri.projet;


import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.uavignon.ceri.projet.data.database.entities.Item;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.projet.RecyclerAdapter.ViewHolder> implements Filterable {

    private static final String TAG = fr.uavignon.ceri.projet.RecyclerAdapter.class.getSimpleName();
    public static final Integer SORT_BY_NAME = 0;
    public static final Integer SORT_BY_POPULARITY = 1;
    public static final Integer SORT_BY_CHRONOLOGY = 2;

    String lastSearch;
    SharedPreferences sharedPreferences;

    private List<Item> itemList;
    private List<Item> itemListFull;
    private CollectionViewModel viewModel;
    private Integer sort;
    private boolean favoriteFirst;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        if(i == 0)
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout_reversed, viewGroup, false);
        else
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public int getItemViewType(int position) {
        return position%2;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemName.setText(itemList.get(i).getName());
        String categories = "";
        boolean addComa = false;
        for(String s : itemList.get(i).getCategories()) {
            if(addComa)
                categories += ", ";
            else addComa = true;
            categories += s;
        }
        viewHolder.itemCategories.setText(categories);
        if(itemList.get(i).getBrand() == null)
            viewHolder.itemBrand.setText("Pas de marque");
        else
            viewHolder.itemBrand.setText("Marque : " + itemList.get(i).getBrand());
        Drawable thumbnail = Drawable.createFromPath(itemList.get(i).getThumbnail());
        viewHolder.itemThumbnail.setImageDrawable(thumbnail);

        Drawable d;
        if(itemList.get(i).getFavorite())
            d = viewHolder.itemStar.getResources().getDrawable(R.drawable.ic_star_full);
        else
            d = viewHolder.itemStar.getResources().getDrawable(R.drawable.ic_star);
        viewHolder.itemStar.setImageDrawable(d);
        viewHolder.itemPopularity.setText(String.valueOf(itemList.get(i).getPopularity()));
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public void setViewModel(CollectionViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public void setItemCatalogue(List<Item> items, int sort, boolean favoriteFirst) {

        if(lastSearch == null || lastSearch == "")
            itemList = items;
        itemListFull = new ArrayList<>(items);
        this.sort = sort;
        this.favoriteFirst = favoriteFirst;
        if(sort == SORT_BY_NAME)
            sortByName();
        else if(sort == SORT_BY_POPULARITY)
            sortByPopularity();
        else if(sort == SORT_BY_CHRONOLOGY)
            sortByChronology();
        if(favoriteFirst)
            favoriteFirst();
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Item> filteredList = new ArrayList<>();

                if(constraint == null || constraint.length() == 0) {
                    filteredList.addAll(itemListFull);
                }
                else {
                    String filter = constraint.toString().toLowerCase().trim();

                    for(Item item : itemListFull) {
                        if (item.getName().toLowerCase().contains(filter)
                                || item.getBrand() != null && item.getBrand().toLowerCase().contains(filter)
                                || item.getYear() != null && item.getYear().toString().contains(filter)
                                || item.getTechnicalDetails() != null && item.getTechnicalDetails().toString().toLowerCase().contains(filter)
                                || item.getCategories().toString().toLowerCase().contains(filter)
                                || item.getDescription().toLowerCase().contains(filter)
                                || item.getTimeFrame().toString().toLowerCase().contains(filter)) {

                            filteredList.add(item);
                        }
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                lastSearch = constraint.toString();

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itemList.clear();
                itemList.addAll((List) results.values);
                notifyDataSetChanged();
            }
        };
    }

    public void sortByName() {

        sort = SORT_BY_NAME;
        Collections.sort(itemList, Item.sortByName);
        Collections.sort(itemListFull, Item.sortByName);
        if(favoriteFirst)
            favoriteFirst();
        notifyDataSetChanged();
    }

    public void sortByPopularity() {

        sort = SORT_BY_POPULARITY;
        Collections.sort(itemList, Item.sortByPopularity);
        Collections.sort(itemListFull, Item.sortByPopularity);
        if(favoriteFirst)
            favoriteFirst();
        notifyDataSetChanged();
    }

    public void sortByChronology() {

        sort = SORT_BY_CHRONOLOGY;
        Collections.sort(itemList, Item.sortByChronology);
        Collections.sort(itemListFull, Item.sortByChronology);
        if(favoriteFirst)
            favoriteFirst();
        notifyDataSetChanged();
    }

    public void favoriteFirst() {

        favoriteFirst = true;
        Collections.sort(itemList, Item.favoriteFirst);
        Collections.sort(itemListFull, Item.favoriteFirst);
        notifyDataSetChanged();
    }

    public void favoriteNotFirst() {

        favoriteFirst = false;
        if(sort.equals(SORT_BY_NAME))
            sortByName();
        else if(sort.equals(SORT_BY_POPULARITY))
            sortByPopularity();
        else if(sort.equals(SORT_BY_CHRONOLOGY))
            sortByChronology();
        notifyDataSetChanged();
    }

    public void setSharedPreference(SharedPreferences sharedPreferences) {

        this.sharedPreferences = sharedPreferences;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView itemThumbnail;
        TextView itemName;
        TextView itemBrand;
        TextView itemCategories;
        ImageView itemStar;
        TextView itemPopularity;

        ViewHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.item_name);
            itemBrand = itemView.findViewById(R.id.item_brand);
            itemCategories = itemView.findViewById(R.id.item_technicalDetails);
            itemThumbnail = itemView.findViewById(R.id.item_thumbnail);
            itemStar = itemView.findViewById(R.id.item_star);
            itemPopularity = itemView.findViewById(R.id.item_popularity);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    String id = RecyclerAdapter.this.itemList.get(getAdapterPosition()).getId();

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(CollectionFragment.LAST_SEARCH, lastSearch);
                    editor.commit();

                    CollectionFragmentDirections.ActionCollectionFragmentToDetailFragment action = CollectionFragmentDirections.actionCollectionFragmentToDetailFragment();
                    action.setItemNum(id);
                    try {
                        Navigation.findNavController(v).navigate(action);
                    }
                    catch(Exception exception) {
                        CategoriesFragmentDirections.ActionCategoriesFragmentToDetailFragment action2 = CategoriesFragmentDirections.actionCategoriesFragmentToDetailFragment();
                        action2.setItemNum(id);
                        Navigation.findNavController(v).navigate(action2);
                    }
                }
            });

            itemStar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Item item = itemList.get(getAdapterPosition());

                    if(item.getFavorite())
                        viewModel.removeFavorite(item);
                    else
                        viewModel.addFavorite(item);
                }
            });
        }
    }
}