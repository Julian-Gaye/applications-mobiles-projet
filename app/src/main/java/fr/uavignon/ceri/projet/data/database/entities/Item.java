package fr.uavignon.ceri.projet.data.database.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.uavignon.ceri.projet.MainActivity;
import fr.uavignon.ceri.projet.data.Converter;

@Entity(tableName = "items")
@TypeConverters(Converter.class)
public class Item {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private String id;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @NonNull
    @ColumnInfo(name = "categories")
    private List<String> categories;

    @NonNull
    @ColumnInfo(name = "description")
    private String description;

    @NonNull
    @ColumnInfo(name = "timeFrame")
    private List<Integer> timeFrame;

    @ColumnInfo(name = "year")
    private Integer year;

    @ColumnInfo(name = "brand")
    private String brand;

    @ColumnInfo(name = "technicalDetails")
    private List<String> technicalDetails;

    @ColumnInfo(name = "pictures")
    private Map<String, String> pictures;

    @ColumnInfo(name = "working")
    private Boolean working;

    @NonNull
    @ColumnInfo(name = "favorite")
    private Boolean favorite;

    @NonNull
    @ColumnInfo(name = "popularity")
    private Long popularity;


    public Item(@NonNull String id, @NonNull String name, @NonNull List<String> categories, @NonNull String description, @NonNull List<Integer> timeFrame, Integer year, String brand, List<String> technicalDetails, Map<String, String> pictures, Boolean working, @NonNull Boolean favorite, @NonNull Long popularity) {
        this.id = id;
        this.name = name;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.pictures = pictures;
        this.working = working;
        this.favorite = favorite;
        this.popularity = popularity;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public List<String> getCategories() {
        return categories;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    @NonNull
    public List<Integer> getTimeFrame() {
        return timeFrame;
    }

    public Integer getYear() {
        return year;
    }

    public String getBrand() {
        return brand;
    }

    public List<String> getTechnicalDetails() {
        return technicalDetails;
    }

    public Map<String, String> getPictures() {
        return pictures;
    }

    public Map<String, String> getPicturesWithAbsolutePath() {

        if(pictures == null)
            return null;

        Map<String, String> picturesWithFullUrl = new HashMap<>();
        for(Map.Entry entry : pictures.entrySet()) {

            picturesWithFullUrl.put(
                    MainActivity.EXTERNAL_CACHE_DIR + "/pictures/" + id + "/" + entry.getKey().toString() + ".jpeg",
                    entry.getValue().toString());
        }
        return picturesWithFullUrl;
    }

    public Boolean getWorking() {
        return working;
    }

    @NonNull
    public String getThumbnail() {

        return MainActivity.EXTERNAL_CACHE_DIR + "/thumbnails/thumbnail_" + id +".png";
    }

    @NonNull
    public Boolean getFavorite() {
        return favorite;
    }

    @NonNull
    public Long getPopularity() {
        return popularity;
    }



    public void setId(@NonNull String id) {
        this.id = id;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public void setCategories(@NonNull List<String> categories) {
        this.categories = categories;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    public void setTimeFrame(@NonNull List<Integer> timeFrame) {
        this.timeFrame = timeFrame;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setTechnicalDetails(List<String> technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public void setPictures(Map<String, String> pictures) {
        this.pictures = pictures;
    }

    public void setWorking(Boolean working) {
        this.working = working;
    }

    public void addFavorite() {
        this.favorite = true;
        this.popularity += 1;
    }

    public void removeFavorite() {
        this.favorite = false;
        this.popularity -= 1;
    }

    public void setPopularity(@NonNull Long popularity) {
        this.popularity = popularity;
    }

    @Override
    public String toString() {
        return  "id : " + id + "\n"
                + "name : " + name + "\n"
                + "categories : " + categories + "\n"
                + "description : "+ description + "\n"
                + "timeFrame : "+ timeFrame + "\n"
                + "year : "+ year + "\n"
                + "brand : " + brand + "\n"
                + "technicalDetails : "+ technicalDetails + "\n"
                + "pictures : "+ pictures + "\n"
                + "working : " + working + "\n"
                + "favori : " + favorite;
    }

    public static Comparator<Item> sortByName = new Comparator<Item>() {
        @Override
        public int compare(Item i1, Item i2) {
            return i1.getName().compareTo(i2.getName());
        }
    };

    public static Comparator<Item> sortByPopularity = new Comparator<Item>() {
        @Override
        public int compare(Item i1, Item i2) {
            return i2.getPopularity().compareTo(i1.getPopularity());
        }
    };

    public static Comparator<Item> sortByChronology = new Comparator<Item>() {
        @Override
        public int compare(Item i1, Item i2) {
            if(i1.getYear() == null) {
                if(i2.getYear() == null)
                    return 0; //Les deux sont null
                return 1; //i1 null mais pas i2 (i1 > i2)
            }
            else if(i2.getYear() == null)
                return -1; //i2 null mais pas i1 (i2 > i1)
            return i1.getYear().compareTo(i2.getYear()); //Aucun des deux n'est null
        }
    };

    public static Comparator<Item> favoriteFirst = new Comparator<Item>() {
        @Override
        public int compare(Item i1, Item i2) {
            return i2.getFavorite().compareTo(i1.getFavorite());
        }
    };
}

