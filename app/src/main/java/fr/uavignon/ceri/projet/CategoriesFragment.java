package fr.uavignon.ceri.projet;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import org.jetbrains.annotations.NotNull;

public class CategoriesFragment extends Fragment {

    private CategoriesViewModel viewModel;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private SectionAdapter adapter;
    private SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        MainActivity activity = (MainActivity)getActivity();
        activity.setMenuMenu();
        sharedPreferences = getContext().getSharedPreferences(MainActivity.SHARED_PREFS, Context.MODE_PRIVATE);
        return inflater.inflate(R.layout.fragment_categories, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(CategoriesViewModel.class);
        listenerSetup();
        observerSetup();
    }

    private void listenerSetup() {
        recyclerView = getView().findViewById(R.id.sectionView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        CollectionViewModel collectionViewModel = new ViewModelProvider(this).get(CollectionViewModel.class);
        adapter = new SectionAdapter(getActivity());
        adapter.setViewModel(collectionViewModel);
        adapter.setSharedPreferences(sharedPreferences);
        recyclerView.setAdapter(adapter);
    }

    private void observerSetup() {
        viewModel.getAllCategories().observe(getViewLifecycleOwner(),
                categories -> {
                    adapter.setCategorieList(categories);
                });

        viewModel.getAllItems().observe(getViewLifecycleOwner(),
                items -> {
                    adapter.setItemList(items);
                });
    }
}
