package fr.uavignon.ceri.projet;


import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.uavignon.ceri.projet.data.database.entities.Item;

public class StaggeredRecyclerAdapter extends RecyclerView.Adapter<StaggeredRecyclerAdapter.ViewHolder> {

    private static final String TAG = fr.uavignon.ceri.projet.RecyclerAdapter.class.getSimpleName();

    private List<Drawable> imageList;
    private List<String> descriptionList;
    private Context context;
    private Handler handler;
    private ProgressBar progressBar;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.image_layout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        String description = descriptionList.get(i).isEmpty() ? "Pas de description" : descriptionList.get(i);
        viewHolder.pictureDescription.setText(description);
        Drawable drawable = imageList.get(i);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        Glide.with(context)
                .load(drawable)
                .override(width / 2) //Pour éviter des lags pendant qu'on scroll
                .into(viewHolder.pictureImage);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return imageList == null ? 0 : imageList.size();
    }

    public void setItem(Item item) {
        handler = new Handler();
        List<String> imageUriList = new ArrayList<>();
        descriptionList = new ArrayList<>();
        for(Map.Entry<String, String> entry : item.getPicturesWithAbsolutePath().entrySet()) {

            imageUriList.add(entry.getKey());
            descriptionList.add(entry.getValue());
        }

        imageList = new ArrayList<>();
        Thread t = new Thread() { //On charge les images dans un thread différent (pour ne pas avoir d'attente quand on clique sur un item)
            @Override
            public void run() {
                super.run();
                for(String imageUri : imageUriList) {
                    imageList.add(Drawable.createFromPath(imageUri));
                }
                handler.post(() -> {
                    notifyDataSetChanged(); //On affiche la galerie une fois que toutes les images sont chargées
                });
            }
        };
        t.start();
    }

    public void setContext(Context context) {

        this.context = context;
    }

    public void setProgressBar(ProgressBar progressBar) {

        this.progressBar = progressBar;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView pictureImage;
        TextView pictureDescription;

        ViewHolder(View itemView) {
            super(itemView);
            pictureImage = itemView.findViewById(R.id.picture_image);
            pictureDescription = itemView.findViewById(R.id.picture_description);
        }
    }
}