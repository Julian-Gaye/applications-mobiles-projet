package fr.uavignon.ceri.projet;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.material.internal.NavigationMenuItemView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.net.UnknownHostException;
import java.util.Collection;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private MainViewModel viewModel;
    private boolean updateButtonPressed;
    private boolean hasLoaded;
    private ProgressBar progress;
    private NavController navController;
    private int currentFragment;
    private SharedPreferences sharedPreferences;

    public static final String SHARED_PREFS = "SharedPrefs";
    public static String EXTERNAL_CACHE_DIR;
    public static String FRAGMENT = "Fragment";

    public static int COLLECTION_FRAGMENT = 0;
    public static int CATEGORIES_FRAGMENT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EXTERNAL_CACHE_DIR = getExternalCacheDir().toString();
        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        updateButtonPressed = false;
        hasLoaded = false;

        sharedPreferences = getApplicationContext().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.side_menu_open, R.string.side_menu_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navController = Navigation.findNavController(this,
                R.id.nav_host_fragment);

        if(savedInstanceState == null) { //On vérifie uniquement au lancement de l'application (pour éviter des problèmes lors de la rotation de l'écran)

            viewModel.loadItems();
            editor.remove(CollectionFragment.LAST_SEARCH); //On enlève la dernière recherche quand on lance l'application
            editor.commit();
        }

        MenuItem menuItem;
        SpannableString spannableString;
        currentFragment = sharedPreferences.getInt(FRAGMENT, COLLECTION_FRAGMENT);

        if (currentFragment == CATEGORIES_FRAGMENT) {

            menuItem = navigationView.getMenu().getItem(1);
            spannableString = new SpannableString(menuItem.getTitle().toString());
            spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 0, spannableString.length(), 0);
            menuItem.setTitle(spannableString);
            if(savedInstanceState == null) {
                NavDirections action = CollectionFragmentDirections.actionCollectionFragmentToCategoriesFragment();
                navController.navigate(action);
            }
        }
        else if (currentFragment == COLLECTION_FRAGMENT) {

            menuItem = navigationView.getMenu().getItem(0);
            spannableString = new SpannableString(menuItem.getTitle().toString());
            spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 0, spannableString.length(), 0);
            menuItem.setTitle(spannableString);
        }
        progress = findViewById(R.id.progress);

        listenerSetup();
    }


    private void listenerSetup() {

        viewModel.getUpdateAvailable().observe(
                this,
                updateAvailable -> {
                    if(updateButtonPressed) {
                        if(updateAvailable == 1) { //Mise à jour disponible
                            updateButtonPressed = false;
                        }
                        else if(updateAvailable == 2) { //Pas de mise à jour disponible
                            Snackbar.make(findViewById(R.id.nav_view), "Aucune mise à jour disponnible", Snackbar.LENGTH_LONG).show();
                            updateButtonPressed = false;
                        }
                    }
                });
        viewModel.isLoading().observe(this,
                isLoading -> {
                    if(isLoading) {
                        Log.d("loading", "Chargement en cours");
                        hasLoaded = true;
                        progress.setVisibility(View.VISIBLE);
                    }
                    else if(hasLoaded) { //Quand on arrive sur le fragment, l'observer est activé donc on doit vérifier que le chargement a bien eu lieu pour afficher la snackbar
                        Log.d("loading", "Chargement terminé");
                        Snackbar.make(findViewById(R.id.nav_view), "Les données ont été mises à jour", Snackbar.LENGTH_LONG).show();
                        hasLoaded = false;
                        progress.setVisibility(View.GONE);
                    }
                });

        viewModel.getError().observe(this,
                error -> {
                    if (error != null) {
                        if (error.getClass() == UnknownHostException.class)
                            Snackbar.make(findViewById(R.id.nav_view), "Impossible de vérifier les mises à jour :\nPas d'accès à internet", Snackbar.LENGTH_LONG).show();
                        else
                            Snackbar.make(findViewById(R.id.nav_view), "Impossible de mettre à jour les données", Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    public void setBackMenu() {

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setMenuMenu() {

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.setToolbarNavigationClickListener(null);
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
        else if(toggle.isDrawerIndicatorEnabled()) //Pour éviter les problèmes de navigation entre les fragments
            finish();
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.button_update) {
            updateButtonPressed = true;
            viewModel.loadItems();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        NavDirections action;
        MenuItem menuItem;
        SpannableString spannableString;
        NavigationView navigationView = findViewById(R.id.nav_view);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        switch(item.getItemId()) {
            case R.id.categories:
                if(currentFragment != CATEGORIES_FRAGMENT) {

                    menuItem = navigationView.getMenu().getItem(0);
                    spannableString = new SpannableString(menuItem.getTitle().toString());
                    spannableString.setSpan(null, 0, spannableString.length(), 0);
                    menuItem.setTitle(spannableString);

                    spannableString = new SpannableString(item.getTitle().toString());
                    spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 0, spannableString.length(), 0);
                    item.setTitle(spannableString);

                    action = CollectionFragmentDirections.actionCollectionFragmentToCategoriesFragment();
                    navController.navigate(action);
                    editor.putInt(FRAGMENT, CATEGORIES_FRAGMENT);
                    editor.commit();
                    currentFragment = CATEGORIES_FRAGMENT;
                }
                break;
            case R.id.collection:
                if(currentFragment != COLLECTION_FRAGMENT) {


                    menuItem = navigationView.getMenu().getItem(1);
                    spannableString = new SpannableString(menuItem.getTitle().toString());
                    spannableString.setSpan(null, 0, spannableString.length(), 0);
                    menuItem.setTitle(spannableString);

                    spannableString = new SpannableString(item.getTitle().toString());
                    spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 0, spannableString.length(), 0);
                    item.setTitle(spannableString);

                    editor.remove(CollectionFragment.LAST_SEARCH); //On enlève la dernière recherche quand on revient sur le fragment
                    editor.commit();

                    action = CategoriesFragmentDirections.actionCategoriesFragmentToCollectionFragment();
                    navController.navigate(action);
                    editor.putInt(FRAGMENT, COLLECTION_FRAGMENT);
                    editor.commit();
                    currentFragment = COLLECTION_FRAGMENT;
                }
                break;
        }
        drawerLayout.close();
        return true;
    }
}