package fr.uavignon.ceri.projet;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import org.jetbrains.annotations.NotNull;

import fr.uavignon.ceri.projet.data.database.entities.Item;

public class DetailFragment extends Fragment {

    private DetailViewModel viewModel;

    private TextView itemName, itemBrand, itemCategories, itemTechnicalDetails, itemTimeFrame, itemYear, itemDescription;
    private ImageView itemThumbnail;
    private RecyclerView recyclerView;
    private StaggeredGridLayoutManager layoutManager;
    private StaggeredRecyclerAdapter adapter;
    private ProgressBar progressBar;

    private Menu menu;
    private Item item;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        MainActivity activity = (MainActivity)getActivity();
        activity.setBackMenu();
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NotNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_detail, menu);
        Drawable d;
        if (item.getFavorite())
            d = getActivity().getResources().getDrawable(R.drawable.ic_star_full);
        else
            d = getActivity().getResources().getDrawable(R.drawable.ic_star);
        menu.findItem(R.id.button_favorite).setIcon(d);
        this.menu = menu;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String itemID = args.getItemNum();
        viewModel.setItem(itemID);

        item = viewModel.getItem().getValue();
        listenerSetup();
        observerSetup();
    }

    private void listenerSetup() {
        recyclerView = getView().findViewById(R.id.stagerredRecyclerView);
        layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        adapter = new StaggeredRecyclerAdapter();
        adapter.setContext(getContext());
        progressBar = getView().findViewById(R.id.progress);
        adapter.setProgressBar(progressBar);
        recyclerView.setAdapter(adapter);

        itemName = getView().findViewById(R.id.item_name);
        itemBrand = getView().findViewById(R.id.item_brand);
        itemCategories = getView().findViewById(R.id.item_categories);
        itemTechnicalDetails = getView().findViewById(R.id.item_technicalDetails);
        itemThumbnail = getView().findViewById(R.id.item_thumbnail);
        itemTimeFrame = getView().findViewById(R.id.item_timeFrame);
        itemYear = getView().findViewById(R.id.item_year);
        itemDescription = getView().findViewById(R.id.item_description);
    }

    private void observerSetup() {
        viewModel.getItem().observe(getViewLifecycleOwner(),
                item -> {
                    if (item != null) {
                        boolean addSeparator;
                        itemName.setText(item.getName() + " " + (item.getWorking() != null ? "(fonctionnel)" : "(non fonctionnel)"));
                        Drawable drawable = Drawable.createFromPath(item.getThumbnail());
                        itemThumbnail.setImageDrawable(drawable);
                        if (item.getBrand() != null)
                            itemBrand.setText(item.getBrand());
                        else
                            itemBrand.setText("Inconnue");
                        if (item.getYear() != null)
                            itemYear.setText(item.getYear().toString());
                        else
                            itemYear.setText("Inconnue");

                        String categories = "";
                        addSeparator = false;
                        for (String s : item.getCategories()) {
                            if (addSeparator)
                                categories += "\n";
                            else
                                addSeparator = true;
                            categories += "- " + s;
                        }
                        itemCategories.setText(categories);

                        if (item.getTechnicalDetails() != null) {
                            String technicalDetails = "";
                            addSeparator = false;
                            for (String s : item.getTechnicalDetails()) {
                                if (addSeparator)
                                    technicalDetails += "\n";
                                else
                                    addSeparator = true;
                                technicalDetails += "- " + s;
                            }
                            itemTechnicalDetails.setText(technicalDetails);
                        }
                        else
                            itemTechnicalDetails.setText("Aucun");

                        String timeFrame = "";
                        addSeparator = false;
                        for (Integer i : item.getTimeFrame()) {
                            if (addSeparator)
                                timeFrame += ", ";
                            else
                                addSeparator = true;
                            timeFrame += i.toString();
                        }
                        itemTimeFrame.setText(timeFrame);
                        if(item.getDescription().equals("")) {
                            TextView descriptionLabel = getView().findViewById(R.id.label_description);
                            descriptionLabel.setVisibility(View.GONE);
                            itemDescription.setVisibility(View.GONE);
                        }
                        else
                            itemDescription.setText(item.getDescription());
                        if(item.getPictures() != null) {
                            adapter.setItem(viewModel.getItem().getValue());
                        }
                        else {
                            TextView picturesLabel = getView().findViewById(R.id.label_pictures);
                            picturesLabel.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {

        if(menuItem.getItemId() == R.id.button_favorite) {

            Drawable d;
            if(item.getFavorite()) {

                viewModel.removeFavorite(item);
                d = getActivity().getResources().getDrawable(R.drawable.ic_star);
            }
            else {

                viewModel.addFavorite(item);
                d = getActivity().getResources().getDrawable(R.drawable.ic_star_full);
            }
            menu.findItem(R.id.button_favorite).setIcon(d);
        }
        return super.onOptionsItemSelected(menuItem);
    }
}