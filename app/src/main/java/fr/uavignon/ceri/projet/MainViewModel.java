package fr.uavignon.ceri.projet;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.projet.data.CerimuseumRepository;

public class MainViewModel extends AndroidViewModel {

    private CerimuseumRepository repository;
    private MutableLiveData<Integer> updateAvailable;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;

    public MainViewModel(@NonNull Application application) {
        super(application);
        repository = CerimuseumRepository.get(application);
        updateAvailable = repository.getUpdateAvailable();
        isLoading = repository.isLoading();
        webServiceThrowable = repository.getError();
    }

    public void loadItems() {

        repository.loadItems();
    }

    public MutableLiveData<Integer> getUpdateAvailable() {

        return updateAvailable;
    }

    public LiveData<Boolean> isLoading() {

        return isLoading;
    }

    public LiveData<Throwable> getError() {

        return webServiceThrowable;
    }
}
