package fr.uavignon.ceri.projet;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import fr.uavignon.ceri.projet.data.CerimuseumRepository;
import fr.uavignon.ceri.projet.data.Section;
import fr.uavignon.ceri.projet.data.database.entities.Categorie;
import fr.uavignon.ceri.projet.data.database.entities.Item;

public class CategoriesViewModel extends AndroidViewModel {

    private CerimuseumRepository repository;
    private LiveData<List<Item>> allItems;
    private LiveData<List<Categorie>> allCategories;

    public CategoriesViewModel(@NonNull Application application) {
        super(application);
        repository = CerimuseumRepository.get(application);
        allItems = repository.getAllItems();
        allCategories = repository.getAllCategories();
    }

    public LiveData<List<Categorie>> getAllCategories() {
        return allCategories;
    }

    public LiveData<List<Item>> getAllItems() {
        return allItems;
    }
}
