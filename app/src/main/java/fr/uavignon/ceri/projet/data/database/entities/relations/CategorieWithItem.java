package fr.uavignon.ceri.projet.data.database.entities.relations;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

import fr.uavignon.ceri.projet.data.database.entities.Categorie;
import fr.uavignon.ceri.projet.data.database.entities.Item;
import fr.uavignon.ceri.projet.data.database.entities.relations.ItemsAndCatogoriesCrossRef;

public class CategorieWithItem {
    @Embedded
    public Categorie categorie;
    @Relation(
            parentColumn = "categorieName",
            entityColumn = "itemId",
            associateBy = @Junction(ItemsAndCatogoriesCrossRef.class)
    )
    public List<Item> items;
}
