package fr.uavignon.ceri.projet.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.projet.data.database.entities.Categorie;
import fr.uavignon.ceri.projet.data.database.entities.Item;

@Dao
public interface CerimuseumDao {

    @Insert
    long insertItem(Item item);

    @Query("DELETE FROM items")
    void deleteAll();

    @Query("SELECT * FROM items ORDER BY name ASC")
    LiveData<List<Item>> getAllItems();

    @Query("SELECT * FROM items ORDER BY name ASC")
    List<Item> getSynchrAllItems();

    @Query("DELETE FROM items WHERE id = :id")
    void deleteItem(String id);

    @Query("SELECT * FROM items WHERE id = :id")
    Item getItemById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item item);

    @Insert
    long insertCategorie(Categorie categorie);

    @Query("SELECT * FROM categories")
    LiveData<List<Categorie>> getAllCategories();
}
