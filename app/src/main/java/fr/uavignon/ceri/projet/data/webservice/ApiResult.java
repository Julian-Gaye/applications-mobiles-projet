package fr.uavignon.ceri.projet.data.webservice;

import android.util.Log;

import java.util.List;
import java.util.Map;

import fr.uavignon.ceri.projet.data.CerimuseumRepository;
import fr.uavignon.ceri.projet.data.database.entities.Categorie;
import fr.uavignon.ceri.projet.data.database.entities.Item;

public class ApiResult {
    public static void updateCollection(Map<String, CollectionResponse> response, CerimuseumRepository repository, List<Item> itemList) {

        for(Item i : itemList) { //On supprime les items qui ont été retirés de la collection
            boolean stillExists = false;
            for(String s : response.keySet()) {
                if(i.getId().equals(s))
                    stillExists = true;
            }
            if(stillExists == false)
                repository.deleteItem(i);
        }

        for(Map.Entry<String, CollectionResponse> entry : response.entrySet()) {

            Item searchedItem = null;
            for(Item i : itemList) {
                if(i.getId().equals(entry.getKey())) {
                    searchedItem = i;
                    break;
                }
            }

            if(searchedItem == null) { //Si l'objet n'existe pas, on le rajoute
                Item item = new Item(entry.getKey(), entry.getValue().name, entry.getValue().categories, entry.getValue().description, entry.getValue().timeFrame, entry.getValue().year, entry.getValue().brand, entry.getValue().technicalDetails, entry.getValue().pictures, entry.getValue().working, false, 0L);
                itemList.add(item);
                repository.insertItem(item);
            }
            else { //Si l'objet existe déjà, on le met à jour
                Item item = searchedItem;
                item.setName(entry.getValue().name);
                item.setCategories(entry.getValue().categories);
                item.setDescription(entry.getValue().description);
                item.setTimeFrame(entry.getValue().timeFrame);
                item.setYear(entry.getValue().year);
                item.setBrand(entry.getValue().brand);
                item.setTechnicalDetails(entry.getValue().technicalDetails);
                item.setPictures(entry.getValue().pictures);
                item.setWorking(entry.getValue().working);
                repository.saveThumbnail(item);
                repository.updateItem(item);
            }
        }

        List<Item> allItems = repository.getAllItems().getValue();
        repository.saveAllThumbnailAndPictures(allItems);
    }

    public static void updatePopularity(Map<String, Long> response, CerimuseumRepository repository, List<Item> itemList) {

        for(Item item : itemList) { //On met la popularité pour chaque objet à 0 car sinon, si la popularité passe de 5 à 0, la popularité affichée restera à 5
            item.setPopularity(0l);
            repository.updateItem(item);
        }

        for (Map.Entry<String, Long> entry : response.entrySet()) {

            Item searchedItem = null;
            for(Item i : itemList) {
                if(i.getId().equals(entry.getKey())) {
                    searchedItem = i;
                    break;
                }
            }

            if(searchedItem != null) {
                Item item = searchedItem;
                item.setPopularity(entry.getValue());
                repository.updateItem(item);
            }
            else
                Log.e("Erreur popularité", "Impossible de trouver l'objet");
        }
    }

    public static void updateCategories(List<String> response, CerimuseumRepository repository) {

        for(String s : response) {
            Categorie categorie = new Categorie(s);
            repository.insertCategorie(categorie);
        }
    }
}