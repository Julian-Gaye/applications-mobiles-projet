package fr.uavignon.ceri.projet.data.database;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.projet.data.database.entities.Categorie;
import fr.uavignon.ceri.projet.data.database.entities.Item;
import fr.uavignon.ceri.projet.data.database.entities.relations.CategorieWithItem;
import fr.uavignon.ceri.projet.data.database.entities.relations.ItemWithCategorie;
import fr.uavignon.ceri.projet.data.database.entities.relations.ItemsAndCatogoriesCrossRef;

@Database(entities = {Item.class, Categorie.class, ItemsAndCatogoriesCrossRef.class}, version = 1, exportSchema = false)
public abstract class CerimuseumRoomDatabase extends RoomDatabase {

    private static final String TAG = CerimuseumRoomDatabase.class.getSimpleName();

    public abstract CerimuseumDao cerimuseumDao();

    private static CerimuseumRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static CerimuseumRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CerimuseumRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    CerimuseumRoomDatabase.class,"cerimuseum_database")
                                    .build();


//                    databaseWriteExecutor.execute(() -> {
//                        CerimuseumDao dao = INSTANCE.cerimuseumDao();
//                        dao.deleteAll();
//                    });


                }
            }
        }
        return INSTANCE;
    }
}
