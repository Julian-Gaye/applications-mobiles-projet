package fr.uavignon.ceri.projet.data.webservice;

import android.graphics.drawable.Drawable;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CerimuseumInterface {

    @Headers("Accept: application/json")
    @GET("/cerimuseum/collection")
    Call<Map<String, CollectionResponse>> getCollection();

    @Headers("Accept: application/json")
    @GET("/cerimuseum/popularity")
    Call<Map<String, Long>> getPopularity();

    @Headers("Accept: application/json")
    @GET("/cerimuseum/collectionversion")
    Call<Float> getCollectionVersion();

    @Headers("Accept: application/json")
    @GET("/cerimuseum/popularityversion")
    Call<Float> getPopularityVersion();

    @Headers("Accept: application/json")
    @POST("/cerimuseum/{itemID}/like")
    Call<Void> like(@Path("itemID") String id);

    @Headers("Accept: application/json")
    @POST("/cerimuseum/{itemID}/dislike")
    Call<Void> dislike(@Path("itemID") String id);

    @Headers("Accept: application/json")
    @GET("/cerimuseum/categories")
    Call<List<String>> getCategories();
}
